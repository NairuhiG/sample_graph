class Processor():
    def __init__(self, alloc_time, proc_id):
        self.alloc_time = alloc_time
        self.proc_id = proc_id

    def set_alloc_time(self, alloc_time):
        self.alloc_time = alloc_time

    def get_alloc_time(self):
        return self.alloc_time

    def set_proc_id(self, proc_id):
        self.proc_id = proc_id

    def get_proc_id(self):
        return self.proc_id