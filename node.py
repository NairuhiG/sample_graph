class Node():
    def __init__(self, id, weight):
        self.id = id
        self.weight = weight
        self.overhead = 0
        self.flatten_value_overhead = 0
        self.st_b_level = 0
        self.b_level = 0
        self.t_level = 0
        self.start_time = 0
        self.alap = 0
        self.cp = 0
        self.node_proc_id = None
        self.node_proc_ids_cl = []
        self.sub_graph = []
        self.sub_makesp_1 = 0
        self.sub_makesp_2 = 0
        self.sub_makesp_3 = 0
        self.sub_makesp_4 = 0
        self.tgff_type = 0
        self.sub_values = []

    def add_sub_value(self, value):
        self.sub_values.append(value)

    def return_sub_value(self):
        return self.sub_values

    def add_sub_graph(self, sg):
        for e in sg.edges:
            e.set_flatten_comm_cost(e.get_communic_cost())
            e.set_communic_cost(0)
            e.set_edge_overhead(0)
        for n in sg.nodes:
            n.set_overhead(0)
        self.sub_graph.append(sg)

    def remove_sub_graph(self, sg):
        self.sub_graph.remove(sg)

    def has_sub_graph(self):
        if self.sub_graph:
            return True
        else:
            print("The node doesn't contain any sub graphs!")

    def get_id(self):
        return self.id

    def set_id(self, id):
        self.id = id

    def get_weight(self):
        return self.weight

    def set_weight(self, weight):
        self.weight = weight

    def set_tgff_type(self, type):
        self.tgff_type = type

    def get_tgff_type(self):
        return self.tgff_type

    def set_overhead(self, o):
        self.overhead = o

    def get_overhead(self):
        return self.overhead

    def set_ex_time_with_overhead(self):
        exec_time = self.weight + self.get_overhead()
        self.set_weight(exec_time)

    def get_st_b_level(self):
        return self.st_b_level

    def set_st_b_level(self, st_b_level):
        self.st_b_level = st_b_level

    def get_b_level(self):
        return self.b_level

    def set_b_level(self, b_level):
        self.b_level = b_level

    def get_t_level(self):
        return self.t_level

    def set_t_level(self, t_level):
        self.t_level = t_level

    def get_start_time(self):
        return self.start_time

    def set_start_time(self, start_time):
        self.start_time = start_time

    def get_alap(self):
        return self.alap

    def set_alap(self, alap):
        self.alap = alap

    def get_cp(self):
        return self.cp

    def set_cp(self, cp):
        self.cp = cp

    def get_node_proc_id(self):
        return self.node_proc_id

    def set_node_proc_id(self, node_proc_id):
        self.node_proc_id = node_proc_id

    def add_node_proc_id_cl(self, node_proc_id):
        self.node_proc_ids_cl.append(node_proc_id)

    def get_node_proc_id_cl(self):
        return self.node_proc_ids_cl