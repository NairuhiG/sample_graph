class Graph():
    def __init__(self):
        self.nodes = []
        self.edges = []
        self.succ_nodes = []
        self.pred_nodes = []
        self.processors = []
        self.levels = []
        self.current_level = 0
        self.successors = []
        self.type = 'DAG'
        self.start_node = None
        self.end_node = None
        self.in_edges = []
        self.out_edges = []

    def add_node(self, n):
        self.nodes.append(n)

    def remove_node(self, n):
        for edge in self.edges:
            if edge.get_successor().get_id() == n.get_id() or edge.get_successor().get_id() == n.get_id():
                self.edges.remove(edge)
        self.nodes.remove(n)

    def add_edge(self, e):
        self.edges.append(e)

    def remove_edge(self, e):
        self.edges.remove(e)

    def set_node_overhead(self, percent):
        sum = 0
        for n in self.nodes:
            sum += n.get_weight()
        avg = sum / len(self.nodes)
        node_overhead = (avg * percent)/100
        for n in self.nodes:
            n.set_overhead(node_overhead)

    def get_weight_by_id(self, id):
        for node in self.nodes:
            if node.get_id() == id:
                w = node.get_weight()
        return w

    def get_comm_cost_by_id(self, id1, id2):
        for edge in self.edges:
            if (edge.get_predecessor().get_id() == id1) & (edge.get_successor().get_id() == id2):
                cc = edge.get_communic_cost()
        return cc

    def get_nodes_count(self):
        return len(self.nodes)

    def get_edges_count(self):
        return len(self.edges)
		
	def calculate_stat_b_level(self, top_node):
        self.current_level += top_node.get_weight()
        successors = []
        for i in range(0, len(self.edges)):
            if self.edges[i].get_predecessor().get_id() == top_node.get_id():
                successors.append(self.edges[i].get_successor())
        if len(successors) > 0:
            for i in range(0, len(successors)):
                self.calculate_stat_b_level(successors[i])
        else:
            self.levels.append(self.current_level)
            self.current_level -= top_node.get_weight()
            return
        self.current_level -= top_node.get_weight()

    def calculate_b_bevel(self, top_node):
        self.current_level += top_node.get_weight()
        successors = []
        for edge in self.edges:
            if edge.get_predecessor().get_id() == top_node.get_id():
                successors.append(edge.get_successor())
        if len(successors) > 0:
            for succ in successors:
                c = self.get_comm_cost_by_id(top_node.get_id(), succ.get_id())
                self.current_level += c
                self.calculate_b_bevel(succ)
                self.current_level -= c
        else:
            self.levels.append(self.current_level)
            self.current_level -= top_node.get_weight()
            return 1
        self.current_level -= top_node.get_weight()
    
	def schedule_tasks(self):
        sorted_by_b_level = []
        for n in self.nodes:
            sorted_by_b_level.append(n)
        for i in range(0, len(sorted_by_b_level) - 1):
            for j in range(0, len(sorted_by_b_level) - i - 1):
                if sorted_by_b_level[j].get_b_level() < sorted_by_b_level[j + 1].get_b_level():
                    swap = sorted_by_b_level[j]
                    sorted_by_b_level[j] = sorted_by_b_level[j + 1]
                    sorted_by_b_level[j + 1] = swap
        for i in range(0, len(sorted_by_b_level)):
            node_est = 0
            optimal_proc = self.processors[0]
            is_allocated = False
            self.get_predecessors(sorted_by_b_level[i].get_id())
            for j in range(len(self.pred_nodes)):
                if self.pred_nodes[j].get_start_time() + self.pred_nodes[j].get_weight() > node_est:
                    node_est = self.pred_nodes[j].get_start_time() + self.pred_nodes[j].get_weight()
            for k in range(0, len(self.processors)):
                if self.processors[k].get_alloc_time() <= node_est:
                    self.allocate_task_2(sorted_by_b_level[i], self.processors[k], node_est)
                    is_allocated = True
                    break
                else:
                    if self.processors[k].get_alloc_time() < optimal_proc.get_alloc_time():
                        optimal_proc = self.processors[k]
            if not is_allocated:
                self.allocate_task_1(sorted_by_b_level[i], optimal_proc)			
				
	def plot_graph(self):
        dot = Digraph(comment='DAG')
        for n in self.nodes:
            i = str(n.get_id())
            dot.node(i)
        for e in self.edges:
            pred = str(e.get_predecessor().get_id())
            succ = str(e.get_successor().get_id())
            dot.edge(pred, succ)
        #print(dot.source)
        dot.render('testing.gv', view=True)