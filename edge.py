class Edge():

    def __init__(self, pred, succ, comm_cost):
        self.pred = pred
        self.succ = succ
        self.comm_cost = comm_cost
        self.edge_overhead = 0
        self.flatten_comm_cost = 0
        self.tgff_type = 0

    def get_communic_cost(self):
        return self.comm_cost

    def set_communic_cost(self, comm_cost):
        self.comm_cost = comm_cost

    def set_tgff_type(self, type):
        self.tgff_type = type

    def get_tgff_type(self):
        return self.tgff_type

    def get_flatten_comm_cost(self):
        return self.flatten_comm_cost

    def set_flatten_comm_cost(self, flatten_comm_cost):
        self.flatten_comm_cost = flatten_comm_cost

    def get_predecessor(self):
        return self.pred

    def get_successor(self):
        return self.succ

    def set_predecessor(self, pred):
        self.pred = pred

    def set_successor(self, succ):
        self.succ = succ

    def set_edge_overhead(self, edge_over):
        self.edge_overhead = edge_over

    def get_edge_overhead(self):
        return self.edge_overhead

    def set_comm_cost_with_overhead(self):
        comm_cost = self.comm_cost + self.get_edge_overhead()
        self.set_communic_cost(comm_cost)



