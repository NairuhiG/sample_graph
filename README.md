# Sample_Graph

This section focuses primarily on the implementation of the simulation tool
for experiments with overhead analyses and reduction, scheduling and clustering
algorithms, as well as applying energy efficiency techniques for multiprocessor
systems. The tool has been developed with the use of Python programming language.
More precisely, the goal is to provide a unified framework enabling modeling,
representation, optimization and implementation of different applications such as
wireless communication signal processing graphs.

This project enables implementation of sample directed acyclic graph(DAG) for experiments with overhead analysis and reduction. DAG consists of nodes and edges. The class node stores all the information regarding tasks, eg. weight, ID, overhead, etc. Edge class has information about predecessor and successor nodes, as well as weight. The tasks can be scheduled on the processors, by taking into consideration the load of processors. Before scheduling the tasks some priority levels should be calculated in order to make a list of ready nodes. All the functionality is presented in the class Graph. 